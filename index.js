'use strict';

const Hapi = require('hapi');


const Server = new Hapi.Server();
Server.connection({
    host: '0.0.0.0',
    port: 8080
});

Server.route({
    method: 'GET',
    path: '/',
    config: {
        handler: (request, reply) => {

            reply("Hello");
        }
    }
});

Server.start((err) => {
    if(err) {
        console.log(err);
        process.exit(1);
    }
    console.log('Starting...');
});